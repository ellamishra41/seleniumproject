package com.pkg;

import org.testng.annotations.Test;

import com.properties.Body;
import com.properties.Resources;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.json.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class ReadingDateFromFile
{
	Properties prop;
	@BeforeTest
		public void setup()
		{
			try {
			prop=new Properties();
			File file=new File("D:\\EclipseLuna\\Selenium\\RestAssured\\src\\com\\properties\\environment.properties");
			FileInputStream fis=new FileInputStream(file);
			prop.load(fis);
			}
		     catch (IOException e) {
				e.printStackTrace();
			}
		}
		//create place api automation(POST)
	    @Test
		public void test1()
		{
			RestAssured.baseURI=prop.getProperty("HOST");
			RequestSpecification request=RestAssured.given();
			request.queryParam("key",prop.getProperty("KEY"));
			request.body(Body.postBody());	
			Response response=request.post(Resources.postResource());
			System.out.println(response.asString());
			JSONObject json1=new JSONObject(response.asString()); //putting response in string
			String place_id1=json1.getString("place_id");//response is converted in to json//getting value of placeid from json object
			System.out.println(place_id1);
			
		   //deleting places
			RestAssured.baseURI=prop.getProperty("HOST");
			RequestSpecification request1=RestAssured.given();
			request1.queryParam("key",prop.getProperty("KEY"));
			//request1.body(Body.deleteBody(place_id1));	
			request1.body("{\n" + 
					"\"place_id\":\"place_id1\"\n" + 
					"}");
			Response response1=request1.post(Resources.postResource());
			System.out.println(response1.body().prettyPrint());
			JSONObject jsonObject1=new JSONObject(response1.asString());
		    String responseOfDelete=jsonObject1.getString("status");
		    String contentType=response1.contentType();
		    System.out.println(contentType);
		    System.out.println(response1.statusCode());
		    System.out.println(responseOfDelete);	
		    
		}	
	}

	

