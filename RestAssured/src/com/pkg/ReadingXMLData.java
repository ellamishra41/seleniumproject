package com.pkg;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.mail.internet.ContentType;
import org.testng.annotations.Test;

import com.properties.Body;


import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

public class ReadingXMLData 
{
  @Test
  public void f1() throws IOException 
  {
	  String path="D:\\EclipseLuna\\Selenium\\RestAssured\\src\\com\\properties\\Post1.xml";
	  RequestSpecification request=RestAssured.given();
	  request.baseUri("http://216.10.245.166");
	  request.queryParam("key", "qaclick123");
	  String body=generateString(path);
	  request.body(body);
	  Response response=request.post("maps/api/place/add/xml");
	  System.out.println(response.asString());
	  XmlPath xmlPath=new XmlPath(response.asString());
	  xmlPath.get("status").equals("OK");
	  
  }
  
  public String generateString(String path) throws IOException
  {
	  //return new String(Files.readAllBytes(Paths.get(path))); //sortest way
	  byte bytes[]=Files.readAllBytes(Paths.get(path));//String conversionz
	  String file=new String(bytes);
	  return file;    
  } 
  
  @Test
  public void f2() throws IOException 
  {
	  //String path="D:\\EclipseLuna\\Selenium\\RestAssured\\src\\com\\properties\\Post1.xml";
	  RequestSpecification request=RestAssured.given();
	  request.baseUri("http://216.10.245.166");
	  request.queryParam("key", "qaclick123");
	  //String body=generateString(path);
	  request.body(Body.xmlBody());
	  Response response=request.post("maps/api/place/add/xml");
	  System.out.println(response.asString());
  }
}
