package com.pkg;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

import org.springframework.ui.context.Theme;
import static org.hamcrest.Matchers.equalTo;

import org.hamcrest.Matcher;

public class Test
{
  public static void main(String args[])
  {
	RestAssured.baseURI="https://maps.googleapis.com";
	given().
	   param("location", "-33.8670522,151.1957362").
	   param("radius", "1500").
	   param("key","AIzaSyBbLepjr5sS5uwzJmB5yQ7tpJA2ZF9tgiY").
	   when().get("maps/api/place/nearbysearch/json").
	   then().contentType(ContentType.JSON).
	   and().body("results[0].name",equalTo("Sydney")).and().
	   header("Server", "scaffolding on HTTPServer2");
	   System.out.println("passed"); 
	 
	   //check with path param
  }
}
