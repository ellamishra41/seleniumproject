package com.pkg;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.internet.ContentType;

import org.apache.http.util.Asserts;
import org.json.JSONObject;
import org.springframework.http.converter.json.GsonBuilderUtils;



import io.restassured.RestAssured;
//import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;




public class GrabResponse
{
	
	
	
	//create place api automation(POST)
	public static void main(String args[])
	{
		/*String b="{\r\n" + 
				"    \"location\":{\r\n" + 
				"        \"lat\" : -38.383494,\r\n" + 
				"        \"lng\" : 33.427362\r\n" + 
				"    },\r\n" + 
				"    \"accuracy\":50,\r\n" + 
				"    \"name\":\"Frontline house\",\r\n" + 
				"    \"phone_number\":\"(+91) 983 893 3937\",\r\n" + 
				"    \"address\" : \"29, side layout, cohen 09\",\r\n" + 
				"    \"types\": [\"shoe park\",\"shop\"],\r\n" + 
				"    \"website\" : \"http://google.com\",\r\n" + 
				"    \"language\" : \"ENGLISH\"\r\n" + 
				"}";
		RestAssured.baseURI="http://216.10.245.166/";
		String response=given().
		queryParam("key","qaclick123").
		body(b).
		when().post("maps/api/place/add/json").
		then().extract().body().asString();
		System.out.println(response);
		JSONObject json = new JSONObject(response);
		String place_id=json.getString("place_id");
		System.out.println(place_id);*/
		
		RestAssured.baseURI="http://216.10.245.166/";
		RequestSpecification request=RestAssured.given();
		

		request.queryParam("key","qaclick123");
		request.body("{\r\n" + 
				"    \"location\":{\r\n" + 
				"        \"lat\" : -38.383494,\r\n" + 
				"        \"lng\" : 33.427362\r\n" + 
				"    },\r\n" + 
				"    \"accuracy\":50,\r\n" + 
				"    \"name\":\"Frontline house\",\r\n" + 
				"    \"phone_number\":\"(+91) 983 893 3937\",\r\n" + 
				"    \"address\" : \"29, side layout, cohen 09\",\r\n" + 
				"    \"types\": [\"shoe park\",\"shop\"],\r\n" + 
				"    \"website\" : \"http://google.com\",\r\n" + 
				"    \"language\" : \"French-IN\"\r\n" + 
				"}");
		
		Response response=request.post("maps/api/place/add/json");
		System.out.println(response.asString());
		JSONObject json1=new JSONObject(response.asString()); //putting response in string
		String place_id1=json1.getString("place_id");//response is converted in to json//getting value of placeid from json object
		System.out.println(place_id1);
		
	//deleting places
		RestAssured.baseURI="http://216.10.245.166/";
		RequestSpecification request1=RestAssured.given();
		request1.queryParam("key","qaclick123");
		request1.body("{\r\n" + 
				"    \"place_id\":\""+place_id1+"\"          \r\n" + 
				"}");
		
		Response response1=request1.post("maps/api/place/delete/json");
		System.out.println(response1.body().prettyPrint());
		JSONObject jsonObject1=new JSONObject(response1.asString());
	    String responseOfDelete=jsonObject1.getString("status");
	    String contentType=response1.contentType();
	    System.out.println(contentType);
	    System.out.println(response1.statusCode());
	    System.out.println(responseOfDelete);
				
	}
	
}
