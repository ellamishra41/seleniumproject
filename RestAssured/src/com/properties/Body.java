package com.properties;

public class Body 
{
public static String postBody()
{
	String str="{\r\n" + 
			"    \"location\":{\r\n" + 
			"        \"lat\" : -38.383494,\r\n" + 
			"        \"lng\" : 33.427362\r\n" + 
			"    },\r\n" + 
			"    \"accuracy\":50,\r\n" + 
			"    \"name\":\"Frontline house\",\r\n" + 
			"    \"phone_number\":\"(+91) 983 893 3937\",\r\n" + 
			"    \"address\" : \"29, side layout, cohen 09\",\r\n" + 
			"    \"types\": [\"shoe park\",\"shop\"],\r\n" + 
			"    \"website\" : \"http://google.com\",\r\n" + 
			"    \"language\" : \"French-IN\"\r\n" + 
			"}";
	return str;
	
}
public static String deleteBody(String place_id1)
{
	String str="{\r\n" + 
			"    \"place_id\":\""+place_id1+"\"          \r\n" + 
			"}";
	return str;
}
public static String xmlBody()
{
	String str="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n" + 
			"<root>\r\n" + 
			"    <location>\r\n" + 
			"        <lat>-38.383494</lat>\r\n" + 
			"        <lng>33.427362</lng>\r\n" + 
			"    </location>\r\n" + 
			"    <accuracy>50</accuracy>\r\n" + 
			"    <name>The Mens store</name>\r\n" + 
			"    <phone_number>(+91) 983 893 3937</phone_number>\r\n" + 
			"    <address>Anna Salai, Chennai</address>\r\n" + 
			"    <types>shoe park</types>\r\n" + 
			"    <types>kadai</types>\r\n" + 
			"    <website>http://google.com</website>\r\n" + 
			"    <language>tamil-IN</language>\r\n" + 
			"</root>";
	return str;
	
}
}
